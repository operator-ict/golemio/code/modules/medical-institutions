CREATE TABLE IF NOT EXISTS institutions (
    "id" varchar(255) NOT NULL,
    "name" varchar(255) NOT NULL,
    "pharmacy_code" varchar(255),
    "institution_code" varchar(255) NOT NULL,
    "geometry" geometry,
    "address" json NOT NULL,
    "district" varchar(255),
    "telephone" json,
    "web" json,
    "email" json,
    "type" json,

    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT institutions_pk PRIMARY KEY (id)
);
CREATE INDEX institutions_geom_idx ON institutions USING gist ("geometry");
CREATE INDEX institutions_district_idx ON institutions USING btree ("district");
CREATE INDEX institutions_updated_at_idx ON institutions USING btree ("updated_at");

CREATE TABLE IF NOT EXISTS opening_hours (
    "id" serial4 NOT NULL,
    "institution_id" varchar(255) NOT NULL,
    "day_of_week" varchar(255) NOT NULL,
    "opens" varchar(255) NOT NULL,
    "closes" varchar(255) NOT NULL,

    -- audit fields,
    create_batch_id int8 NULL,
    created_at timestamptz NULL,
    created_by varchar(150) NULL,
    update_batch_id int8 NULL,
    updated_at timestamptz NULL,
    updated_by varchar(150) NULL,

    CONSTRAINT opening_hours_pk PRIMARY KEY (id),
    CONSTRAINT opening_hours_fk FOREIGN KEY ("institution_id")
        REFERENCES institutions("id")
        ON DELETE CASCADE
);
