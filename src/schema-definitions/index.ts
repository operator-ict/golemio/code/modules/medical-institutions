import { healthCareJsonSchema, pharmaciesFilesJsonSchema } from "#sch/datasources";

const forExport: any = {
    name: "MedicalInstitutions",
    pgSchema: "medical_institutions",
    datasources: {
        healthCare: {
            name: "HealthCareDatasource",
            jsonSchema: healthCareJsonSchema,
        },
        pharmaciesFiles: {
            name: "PharmaciesFilesDatasource",
            jsonSchema: pharmaciesFilesJsonSchema,
        },
    },
    definitions: {
        medicalInstitutions: {
            name: "MedicalInstitutions",
            pgTableName: "institutions",
        },
        medicalInstitutionsOpeningHours: {
            name: "MedicalInstitutionsOpeningHours",
            pgTableName: "opening_hours",
        },
    },
};

export { forExport as MedicalInstitutions };
