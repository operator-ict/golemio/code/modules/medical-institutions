import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface IHealthCareInput {
    CisloDomovniOrientacni?: string;
    CisloDomovniOrientacniSidlo?: string;
    DruhPece?: string;
    DruhZarizeni: string;
    FormaPece?: string;
    Ico?: string;
    Kod?: string;
    Kraj?: string;
    KrajCode?: string;
    KrajCodeSidlo?: string;
    Lat: string;
    Lng: string;
    MistoPoskytovaniId?: string;
    NazevZarizeni: string;
    Obec?: string;
    ObecSidlo?: string;
    OborPece?: string;
    OdbornyZastupce?: string;
    Okres?: string;
    OkresCode?: string;
    OkresCodeSidlo?: string;
    PoskytovatelEmail?: string;
    PoskytovatelFax?: string;
    PoskytovatelTelefon?: string;
    PoskytovatelWeb?: string;
    PravniFormaKod?: string;
    Psc?: string;
    PscSidlo?: string;
    SpravniObvod?: string;
    TypOsoby?: string;
    Ulice?: string;
    UliceSidlo?: string;
    ZdravotnickeZarizeniId: string;
}

export const healthCareJsonSchema: JSONSchemaType<IHealthCareInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            CisloDomovniOrientacni: { type: "string", nullable: true },
            CisloDomovniOrientacniSidlo: { type: "string", nullable: true },
            DruhPece: { type: "string", nullable: true },
            DruhZarizeni: { type: "string" },
            FormaPece: { type: "string", nullable: true },
            Ico: { type: "string", nullable: true },
            Kod: { type: "string", nullable: true },
            Kraj: { type: "string", nullable: true },
            KrajCode: { type: "string", nullable: true },
            KrajCodeSidlo: { type: "string", nullable: true },
            Lat: { type: "string" },
            Lng: { type: "string" },
            MistoPoskytovaniId: { type: "string", nullable: true },
            NazevZarizeni: { type: "string" },
            Obec: { type: "string", nullable: true },
            ObecSidlo: { type: "string", nullable: true },
            OborPece: { type: "string", nullable: true },
            OdbornyZastupce: { type: "string", nullable: true },
            Okres: { type: "string", nullable: true },
            OkresCode: { type: "string", nullable: true },
            OkresCodeSidlo: { type: "string", nullable: true },
            PoskytovatelEmail: { type: "string", nullable: true },
            PoskytovatelFax: { type: "string", nullable: true },
            PoskytovatelTelefon: { type: "string", nullable: true },
            PoskytovatelWeb: { type: "string", nullable: true },
            PravniFormaKod: { type: "string", nullable: true },
            Psc: { type: "string", nullable: true },
            PscSidlo: { type: "string", nullable: true },
            SpravniObvod: { type: "string", nullable: true },
            TypOsoby: { type: "string", nullable: true },
            Ulice: { type: "string", nullable: true },
            UliceSidlo: { type: "string", nullable: true },
            ZdravotnickeZarizeniId: { type: "string" },
        },
        required: ["DruhZarizeni", "Lat", "Lng", "NazevZarizeni", "ZdravotnickeZarizeniId"],
        additionalProperties: false,
    },
};
