import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";

export interface IPharmaciesFilesInput {
    filepath: string;
    mtime: Date;
    name: string;
    path: string;
}

export const pharmaciesFilesJsonSchema: JSONSchemaType<IPharmaciesFilesInput[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            filepath: { type: "string" },
            mtime: { type: "object", required: [] },
            name: { type: "string" },
            path: { type: "string" },
        },
        required: ["filepath", "mtime", "name", "path"],
        additionalProperties: true,
    },
};
