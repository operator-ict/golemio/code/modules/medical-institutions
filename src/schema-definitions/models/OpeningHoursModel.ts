import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IOpeningHours } from "#sch/OpeningHours";

export class OpeningHoursModel extends Model<IOpeningHours> implements IOpeningHours {
    declare id: number;
    declare institution_id: string;
    declare day_of_week: string;
    declare opens: string;
    declare closes: string;

    public static attributeModel: ModelAttributes<OpeningHoursModel, IOpeningHours> = {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        institution_id: DataTypes.INTEGER,
        day_of_week: DataTypes.STRING(255),
        opens: DataTypes.STRING(255),
        closes: DataTypes.STRING(255),
    };

    public static updateAttributes = Object.keys(OpeningHoursModel.attributeModel)
        .filter((att) => att !== "id")
        .concat("updated_at") as Array<keyof IOpeningHours>;

    public static jsonSchema: JSONSchemaType<IOpeningHours[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "integer" },
                institution_id: { type: "string" },
                day_of_week: { type: "string" },
                opens: { type: "string" },
                closes: { type: "string" },
            },
            required: ["institution_id", "day_of_week", "opens", "closes"],
        },
    };
}
