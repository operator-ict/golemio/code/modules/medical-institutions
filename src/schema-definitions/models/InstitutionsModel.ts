import { Point } from "@golemio/core/dist/shared/geojson";
import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { DataTypes, Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { SharedSchemaProvider } from "@golemio/core/dist/schema-definitions";
import { IPostalAddress } from "@golemio/core/dist/integration-engine";
import { IInstitution, IInstitutionType } from "#sch/Institutions";

export class InstitutionsModel extends Model<IInstitution> implements IInstitution {
    declare id: string;
    declare name: string;
    declare institution_code: string;
    declare pharmacy_code: string;
    declare geometry: Point;
    declare address: IPostalAddress;
    declare district: string | null;
    declare web: string[];
    declare email: string[];
    declare telephone: string[];
    declare type: IInstitutionType;

    public static attributeModel: ModelAttributes<InstitutionsModel, IInstitution> = {
        id: {
            type: DataTypes.STRING(255),
            primaryKey: true,
        },
        name: DataTypes.STRING(255),
        institution_code: DataTypes.STRING(255),
        pharmacy_code: DataTypes.STRING(255),
        geometry: DataTypes.GEOMETRY,
        address: DataTypes.JSON,
        district: DataTypes.STRING(255),
        web: DataTypes.JSON,
        email: DataTypes.JSON,
        telephone: DataTypes.JSON,
        type: DataTypes.JSON,
    };

    public static updateAttributes = Object.keys(InstitutionsModel.attributeModel)
        .filter((att) => att !== "id")
        .concat("updated_at") as Array<keyof IInstitution>;

    public static jsonSchema: JSONSchemaType<IInstitution[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: { type: "string" },
                name: { type: "string" },
                institution_code: { type: "string" },
                pharmacy_code: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                geometry: { $ref: "#/definitions/geometry" },
                address: { type: "object" },
                district: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                web: { type: "array", items: { type: "string" } },
                email: { type: "array", items: { type: "string" } },
                telephone: { type: "array", items: { type: "string" } },
                type: {
                    type: "object",
                    properties: {
                        id: { type: "string" },
                        group: { type: "string" },
                        description: { oneOf: [{ type: "string" }, { type: "null", nullable: true }] },
                    },
                },
            },
            required: ["id", "name", "institution_code"],
        },
        definitions: {
            // @ts-expect-error
            geometry: SharedSchemaProvider.Geometry,
        },
    };
}
