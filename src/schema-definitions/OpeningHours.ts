export interface IOpeningHours {
    id: number;
    institution_id: string;
    day_of_week: string;
    opens: string;
    closes: string;
}
