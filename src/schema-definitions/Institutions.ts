import { Point } from "@golemio/core/dist/shared/geojson";
import { IPostalAddress } from "@golemio/core/dist/integration-engine";
import { IOpeningHours } from "#sch/OpeningHours";

export interface IInstitutionType {
    description: string | null;
    group: string;
    id: string;
}

export interface IInstitution {
    id: string;
    name: string;
    pharmacy_code: string | null;
    institution_code: string | null;
    geometry: Point;
    address: IPostalAddress;
    district: string | null;
    telephone: string[];
    web: string[];
    email: string[];
    type: IInstitutionType;
    opening_hours?: IOpeningHours[];
}
