import slug from "slugify";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { MedicalInstitutions } from "#sch/index";
import { TransformationHelper } from "#ie/transformations/helpers/TransformationHelper";
import { IInstitution } from "#sch/Institutions";
import { IHealthCareInput } from "#sch/datasources";

export class HealthCareTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MedicalInstitutions.datasources.healthCare.name;
    }

    protected transformElement = async (element: IHealthCareInput): Promise<IInstitution> => {
        return {
            id: slug(element.ZdravotnickeZarizeniId + "-" + element.NazevZarizeni, { lower: true, remove: /[*+~.,()'"!:@]/g }),
            name: element.NazevZarizeni,
            pharmacy_code: null,
            institution_code: element.ZdravotnickeZarizeniId,
            geometry: {
                coordinates: [parseFloat(element.Lng), parseFloat(element.Lat)],
                type: "Point",
            },
            address: TransformationHelper.getAddressHealthCare(element),
            district: null,
            telephone: element.PoskytovatelTelefon ? element.PoskytovatelTelefon.split(",").map((el) => el.trim()) : [],
            web: element.PoskytovatelWeb ? element.PoskytovatelWeb.split(",").map((el) => el.trim()) : [],
            email: element.PoskytovatelEmail ? element.PoskytovatelEmail.split(",").map((el) => el.trim()) : [],
            type: {
                description: element.DruhZarizeni,
                group: "health_care",
                id: TransformationHelper.getTypeId(element.DruhZarizeni),
            },
        };
    };
}
