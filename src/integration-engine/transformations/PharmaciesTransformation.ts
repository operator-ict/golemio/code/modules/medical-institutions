import { parse as fastcsvParse } from "fast-csv";
import iconv from "iconv-lite";
import slug from "slugify";
import { Readable } from "stream";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { IFile, log } from "@golemio/core/dist/integration-engine";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine/transformations";
import { MedicalInstitutions } from "#sch/index";
import { TransformationHelper } from "#ie/transformations/helpers/TransformationHelper";
import { IInstitution } from "#sch/Institutions";
import { IOpeningHours } from "#sch/OpeningHours";

interface IPharmacyListInput {
    KOD_PRACOVISTE: string;
    NAZEV: string;
    MESTO: string;
    KOD_LEKARNY: string;
    TELEFON: string;
    WWW: string;
    EMAIL: string;
    TYP_LEKARNY: string;
}

interface IPharmacyTypeInput {
    TYP_LEKARNY: string;
    NAZEV: string;
}

interface IPharmacyOpeningHoursInput {
    KOD_PRACOVISTE: string;
    DEN: string;
    OD: string;
    DO: string;
}

export interface IMedicalInstitutionsTransformation {
    institutions: IInstitution[];
    openingHours: IOpeningHours[];
}

export class PharmaciesTransformation extends BaseTransformation implements ITransformation {
    public name: string;

    constructor() {
        super();
        this.name = MedicalInstitutions.datasources.pharmaciesFiles.name;
    }

    public transform = async (files: IFile[]): Promise<IMedicalInstitutionsTransformation> => {
        const records: Record<string, any> = {
            institutions: [],
            openingHours: {},
            types: {},
        };

        for await (const file of files) {
            switch (file.name) {
                case "lekarny_seznam":
                    records.institutions = await this.transformFile<IPharmacyListInput>(file);
                    records.institutions = records.institutions.filter(
                        (i: IPharmacyListInput) => i.MESTO.indexOf("Praha") !== -1
                    );
                    break;
                case "lekarny_typ":
                    const types = await this.transformFile<IPharmacyTypeInput>(file);
                    for (const type of types) {
                        records.types[type.TYP_LEKARNY] = type.NAZEV;
                    }
                    break;
                case "lekarny_prac_doba":
                    const hours = await this.transformFile<IPharmacyOpeningHoursInput>(file);
                    for (const hour of hours) {
                        if (hour.OD && hour.DO && hour.OD !== hour.DO) {
                            if (!records.openingHours[hour.KOD_PRACOVISTE]) {
                                records.openingHours[hour.KOD_PRACOVISTE] = [];
                            }
                            records.openingHours[hour.KOD_PRACOVISTE].push({
                                closes: hour.DO,
                                day_of_week: TransformationHelper.getDayLabel(hour.DEN),
                                opens: hour.OD,
                            });
                        }
                    }
                    break;
                default:
                    log.warn("Unknown filename: " + file.name);
                    break;
            }
        }

        const institutionsTransformed = [];
        const openingHoursTransformed = [];
        for (const institution of records.institutions) {
            const ri = this.transformElement(institution);
            ri.type.description = records.types[ri.type.id];
            institutionsTransformed.push(ri);
            const roh = [];
            for (const openingHours of records?.openingHours?.[ri.institution_code as string] || []) {
                roh.push({
                    ...openingHours,
                    institution_id: ri.id,
                });
            }

            openingHoursTransformed.push(...roh);
        }

        return {
            institutions: institutionsTransformed,
            openingHours: openingHoursTransformed,
        };
    };

    protected transformElement = (element: IPharmacyListInput): IInstitution => {
        return {
            id: slug(element.KOD_PRACOVISTE + "-" + element.NAZEV, { lower: true, remove: /[*+~.,()'"!:@]/g }),
            name: element.NAZEV,
            pharmacy_code: element.KOD_LEKARNY,
            institution_code: element.KOD_PRACOVISTE,
            geometry: {
                coordinates: [0, 0], // WARNING coordinates must be added retroactively
                type: "Point",
            },
            address: TransformationHelper.getAddressPharmacies(element),
            district: null,
            telephone: element.TELEFON ? element.TELEFON.trim().split(",") : [],
            web: element.WWW ? element.WWW.trim().split(",") : [],
            email: element.EMAIL ? element.EMAIL.trim().split(",") : [],
            type: {
                description: null,
                group: "pharmacies",
                id: element.TYP_LEKARNY,
            },
        };
    };

    private transformFile = async <T>(file: IFile): Promise<T[]> => {
        const readable = new Readable({
            read: () => null,
        });
        readable.push(iconv.decode(Buffer.from(file.data as string, "hex"), "win1250"));
        readable.push(null);

        return new Promise((resolve, reject) => {
            const result: any[] = [];
            readable
                .pipe(fastcsvParse({ delimiter: ";", headers: true, trim: true }))
                .on("error", (error) => {
                    reject(new GeneralError("Error while parsing source data.", this.constructor.name, error));
                })
                .on("data", (row) => {
                    result.push(row);
                })
                .on("end", () => {
                    return resolve(result);
                });
        });
    };
}
