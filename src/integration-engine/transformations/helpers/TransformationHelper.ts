import { IPostalAddress } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";

export enum OpeningHoursDays {
    PO = "Monday",
    UT = "Tuesday",
    ST = "Wednesday",
    CT = "Thursday",
    PA = "Friday",
    SO = "Saturday",
    NE = "Sunday",
    SVATEK = "PublicHolidays",
}

export class TransformationHelper {
    public static getAddressPharmacies = (address: any): IPostalAddress => {
        return {
            address_country: "Česko",
            address_formatted: `${address.ULICE}, ${address.PSC} ${address.MESTO}, Česko`,
            address_locality: address.MESTO,
            postal_code: address.PSC,
            street_address: `${address.ULICE}`,
        };
    };

    public static getAddressHealthCare = (address: any): IPostalAddress => {
        return {
            address_country: "Česko",
            address_formatted: `${address.Ulice} ${address.CisloDomovniOrientacni}, ${address.Psc} ${address.Obec}, Česko`,
            address_locality: address.Obec,
            postal_code: address.Psc,
            street_address: `${address.Ulice} ${address.CisloDomovniOrientacni}`,
        };
    };

    public static getTypeId = (type: string): string => {
        switch (type) {
            case "Fakultní nemocnice":
                return "ZS-FN";
            case "Nemocnice":
                return "ZS-N";
            case "Nemocnice následné péče":
                return "ZS-NNP";
            case "Ostatní ambulantní zařízení":
                return "ZS-OAZ";
            case "Ostatní zdravotnická zařízení":
                return "ZS-OZZ";
            case "Ostatní zvláštní zdravotnická zařízení":
                return "ZS-OZZZ";
            case "Výdejna zdravotnických prostředků":
                return "ZS-VZP";
            case "Záchytná stanice":
                return "ZS-ZS";
            case "Zdravotní záchranná služba":
                return "ZS-ZZS";
            case "Zdravotnické středisko":
                return "ZS-ZDS";
            default:
                return "ZS";
        }
    };

    public static getDayLabel = (day: string): string => {
        switch (day) {
            case "PO":
                return OpeningHoursDays.PO;
            case "UT":
                return OpeningHoursDays.UT;
            case "ST":
                return OpeningHoursDays.ST;
            case "CT":
                return OpeningHoursDays.CT;
            case "PA":
                return OpeningHoursDays.PA;
            case "SO":
                return OpeningHoursDays.SO;
            case "NE":
                return OpeningHoursDays.NE;
            case "SVATEK":
                return OpeningHoursDays.SVATEK;
            default:
                throw new GeneralError("Error: Unknown day format", this.constructor.name);
        }
    };
}
