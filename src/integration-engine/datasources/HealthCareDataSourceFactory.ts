import { CSVDataTypeStrategy, DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { config } from "@golemio/core/dist/integration-engine/config";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MedicalInstitutions } from "#sch";

export class HealthCareDataSourceFactory {
    public static getDataSource(): DataSource {
        const csvStrategy = new CSVDataTypeStrategy({
            fastcsvParams: { headers: true },
            subscribe: (json: any) => {
                delete json.CisloDomovniOrientacniSidlo;
                delete json.DruhPece;
                delete json.FormaPece;
                delete json.Ico;
                delete json.Kod;
                delete json.Kraj;
                delete json.KrajCodeSidlo;
                delete json.MistoPoskytovaniId;
                delete json.ObecSidlo;
                delete json.OborPece;
                delete json.OdbornyZastupce;
                delete json.Okres;
                delete json.OkresCode;
                delete json.OkresCodeSidlo;
                delete json.PoskytovatelFax;
                delete json.PravniFormaKod;
                delete json.PscSidlo;
                delete json.SpravniObvod;
                delete json.TypOsoby;
                delete json.UliceSidlo;
                return json;
            },
        });
        csvStrategy.setFilter((item) => {
            return (
                item.KrajCode === "CZ010" &&
                item.Lat &&
                item.Lng &&
                [
                    "Fakultní nemocnice",
                    "Nemocnice",
                    "Nemocnice následné péče",
                    "Ostatní ambulantní zařízení",
                    "Ostatní zdravotnická zařízení",
                    "Ostatní zvláštní zdravotnická zařízení",
                    "Výdejna zdravotnických prostředků",
                    "Záchytná stanice",
                    "Zdravotní záchranná služba",
                    "Zdravotnické středisko",
                ].indexOf(item.DruhZarizeni) !== -1
            );
        });

        return new DataSource(
            MedicalInstitutions.datasources.healthCare.name,
            new HTTPFetchProtocolStrategy({
                headers: {},
                method: "GET",
                url: config.datasources.MedicalInstitutionsHealthCare,
                responseType: "text",
            }),
            csvStrategy,
            new JSONSchemaValidator(
                MedicalInstitutions.datasources.healthCare.name + "Validator",
                MedicalInstitutions.datasources.healthCare.jsonSchema
            )
        );
    }
}
