import { MedicalInstitutions } from "#sch";
import { config } from "@golemio/core/dist/integration-engine/config";
import { DataSource } from "@golemio/core/dist/integration-engine/datasources";
import { ZipFileDataTypeStrategy } from "@golemio/core/dist/integration-engine/datasources/datatype-strategy/ZipFileDataTypeStrategy";
import { HTTPFetchProtocolStrategy } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPFetchProtocolStrategy";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class PharmaciesDataSourceFactory {
    public static getDataSource(): DataSource {
        return new DataSource(
            MedicalInstitutions.datasources.pharmaciesFiles.name,
            new HTTPFetchProtocolStrategy({
                method: "GET",
                responseType: "arrayBuffer",
                url: config.datasources.MedicalInstitutionsPharmacies,
            }),
            new ZipFileDataTypeStrategy({
                name: "MedicalInstitutionsPharmaciesStrat",
                whitelistedFiles: ["lekarny_prac_doba.csv", "lekarny_seznam.csv", "lekarny_typ.csv"],
            }),
            new JSONSchemaValidator(
                MedicalInstitutions.datasources.pharmaciesFiles.name + "Validator",
                MedicalInstitutions.datasources.pharmaciesFiles.jsonSchema
            )
        );
    }
}
