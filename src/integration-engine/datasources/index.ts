/* ie/datasources/index.ts */
export * from "./HealthCareDataSourceFactory";
export * from "./PharmaciesDataSourceFactory";
