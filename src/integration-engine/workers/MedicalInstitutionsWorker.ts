import { AbstractWorker } from "@golemio/core/dist/integration-engine/workers";
import { RefreshDataInDBTask, UpdateGeoAndDistrictTask } from "#ie/workers/tasks";

export class MedicalInstitutionsWorker extends AbstractWorker {
    protected readonly name = "MedicalInstitutions";

    constructor() {
        super();
        this.registerTask(new RefreshDataInDBTask(this.getQueuePrefix()));
        this.registerTask(new UpdateGeoAndDistrictTask(this.getQueuePrefix()));
    }
}
