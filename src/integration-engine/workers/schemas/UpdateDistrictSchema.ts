import {
    ArrayMaxSize,
    ArrayMinSize,
    IsNumber,
    IsObject,
    IsString,
    ValidateIf,
    ValidateNested,
} from "@golemio/core/dist/shared/class-validator";
import { Type } from "@golemio/core/dist/shared/class-transformer";
import { Point } from "@golemio/core/dist/shared/geojson";

export interface IUpdateDistrictInput {
    id: string;
    geometry: Point;
    district: string | null;
}

class IGeometryDTO implements Point {
    @IsString()
    type!: "Point";

    @ArrayMinSize(2)
    @ArrayMaxSize(2)
    @IsNumber({}, { each: true })
    coordinates!: number[];
}

export class UpdateDistrictValidationSchema implements IUpdateDistrictInput {
    @IsString()
    id!: string;

    @IsObject()
    @ValidateNested()
    @Type(() => IGeometryDTO)
    geometry!: Point;

    @IsString()
    @ValidateIf((object, value) => value !== null)
    district!: string | null;
}
