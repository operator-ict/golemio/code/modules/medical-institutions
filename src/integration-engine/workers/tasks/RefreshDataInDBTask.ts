import { AbstractEmptyTask, QueueManager, RedisModel } from "@golemio/core/dist/integration-engine";
import { DataSource, IFile } from "@golemio/core/dist/integration-engine/datasources";
import { HealthCareDataSourceFactory, PharmaciesDataSourceFactory } from "#ie/datasources";
import { HealthCareTransformation, PharmaciesTransformation } from "#ie/transformations";
import { MedicalInstitutionsRepository } from "#ie/repositories";
import { MedicalInstitutions } from "#sch";
import { IInstitution } from "#sch/Institutions";

export class RefreshDataInDBTask extends AbstractEmptyTask {
    public readonly queueName = "refreshDataInDB";
    public readonly queueTtl = 25 * 24 * 60 * 60 * 1000; // 25 days

    private healthCareDataSource: DataSource;
    private pharmaciesDataSource: DataSource;
    private healthCareTransformation: HealthCareTransformation;
    private pharmaciesTransformation: PharmaciesTransformation;
    private redisModel: RedisModel;
    private repository: MedicalInstitutionsRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.healthCareDataSource = HealthCareDataSourceFactory.getDataSource();
        this.pharmaciesDataSource = PharmaciesDataSourceFactory.getDataSource();
        this.healthCareTransformation = new HealthCareTransformation();
        this.pharmaciesTransformation = new PharmaciesTransformation();
        this.repository = new MedicalInstitutionsRepository();
        this.redisModel = new RedisModel(
            MedicalInstitutions.datasources.pharmaciesFiles.name + "Model",
            {
                isKeyConstructedFromData: false,
                prefix: "files",
            },
            null
        );
    }

    protected async execute(): Promise<void> {
        const healthCareData = await this.healthCareDataSource.getAll();
        const transformedHealthCareData = await this.healthCareTransformation.transform(healthCareData);

        const pharmaciesFileData = await this.pharmaciesDataSource.getAll();
        await Promise.all(
            pharmaciesFileData.map(async (files: IFile) => {
                files.data = await this.redisModel.hget(files.filepath);
                return files;
            })
        );
        const transformedPharmaciesData = await this.pharmaciesTransformation.transform(pharmaciesFileData);

        const transformedData = {
            institutions: transformedHealthCareData.concat(transformedPharmaciesData.institutions),
            openingHours: transformedPharmaciesData.openingHours,
        };

        await this.repository.saveInstitutions(transformedData);

        const promises = transformedData.institutions.map((data: IInstitution) => {
            return QueueManager.sendMessageToExchange(this.queuePrefix, "updateGeoAndDistrict", {
                id: data.id,
                geometry: data.geometry,
                district: data.district,
            });
        });
        await Promise.all(promises);
    }
}
