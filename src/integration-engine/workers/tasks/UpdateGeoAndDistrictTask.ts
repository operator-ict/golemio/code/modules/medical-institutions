import "@golemio/core/dist/shared/_global";
import { AbstractTask, GeocodeApi, log } from "@golemio/core/dist/integration-engine";
import { RecoverableError } from "@golemio/core/dist/shared/golemio-errors";
import CityDistrictsModel from "@golemio/city-districts/dist/integration-engine/repositories/CityDistrictPostgresRepository";
import { IUpdateDistrictInput, UpdateDistrictValidationSchema } from "#ie/workers/schemas/UpdateDistrictSchema";
import { MedicalInstitutionsRepository } from "#ie/repositories";

export class UpdateGeoAndDistrictTask extends AbstractTask<IUpdateDistrictInput> {
    public readonly queueName = "updateGeoAndDistrict";
    public readonly queueTtl = 25 * 24 * 60 * 60 * 1000; // 25 days
    public readonly schema = UpdateDistrictValidationSchema;

    public repository: MedicalInstitutionsRepository;
    private cityDistrictsModel: CityDistrictsModel;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.repository = new MedicalInstitutionsRepository();
        this.cityDistrictsModel = new CityDistrictsModel();
    }

    protected async execute(msg: IUpdateDistrictInput) {
        try {
            const data = await this.repository.findOne({
                where: { id: msg.id },
                raw: true,
            });

            if (data.geometry.coordinates[0] === 0 && data.geometry.coordinates[1] === 0) {
                try {
                    data.geometry.coordinates = await GeocodeApi.getGeoByAddress(
                        data.address.street_address,
                        data.address.address_locality.replace(/[0-9]/g, "").trim()
                    );
                } catch (err) {
                    await this.repository["sequelizeModel"].destroy({ where: { id: msg.id } });
                    log.debug("Address by geo was not found. Object '" + data.id + "' removed.");
                    throw new RecoverableError("Error while updating geo.", this.constructor.name, err);
                }
            }

            if (
                !data.district ||
                msg.geometry.coordinates[0] !== data.geometry.coordinates[0] ||
                msg.geometry.coordinates[1] !== data.geometry.coordinates[1]
            ) {
                try {
                    data.district = await this.cityDistrictsModel.getDistrict(
                        data.geometry.coordinates[0],
                        data.geometry.coordinates[1]
                    );
                } catch (err) {
                    throw new RecoverableError("Error while updating district.", this.constructor.name, err);
                }
            }

            await this.repository.save([data]);
        } catch (err) {
            throw new RecoverableError("Error while updating city district.", this.constructor.name, err);
        }
    }
}
