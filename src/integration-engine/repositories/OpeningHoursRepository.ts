import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { MedicalInstitutions } from "#sch";
import { OpeningHoursModel } from "#sch/models";

export class OpeningHoursRepository extends PostgresModel implements IModel {
    constructor() {
        super(
            MedicalInstitutions.definitions.medicalInstitutionsOpeningHours.name + "Repository",
            {
                outputSequelizeAttributes: OpeningHoursModel.attributeModel,
                pgTableName: MedicalInstitutions.definitions.medicalInstitutionsOpeningHours.pgTableName,
                pgSchema: MedicalInstitutions.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                MedicalInstitutions.definitions.medicalInstitutionsOpeningHours.name + "Validator",
                OpeningHoursModel.jsonSchema
            )
        );
    }
}
