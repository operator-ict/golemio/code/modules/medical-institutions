import { IModel, PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { Transaction, Op } from "@golemio/core/dist/shared/sequelize";
import { OpeningHoursRepository } from "#ie/repositories";
import { MedicalInstitutions } from "#sch";
import { InstitutionsModel, OpeningHoursModel } from "#sch/models";
import { IOpeningHours } from "#sch/OpeningHours";
import { IInstitution } from "#sch/Institutions";
import { IMedicalInstitutionsTransformation } from "#ie/transformations";

export class MedicalInstitutionsRepository extends PostgresModel implements IModel {
    private readonly openingHoursRepository: OpeningHoursRepository;

    constructor() {
        super(
            MedicalInstitutions.definitions.medicalInstitutions.name + "Repository",
            {
                outputSequelizeAttributes: InstitutionsModel.attributeModel,
                pgTableName: MedicalInstitutions.definitions.medicalInstitutions.pgTableName,
                pgSchema: MedicalInstitutions.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(
                MedicalInstitutions.definitions.medicalInstitutions.name + "Validator",
                InstitutionsModel.jsonSchema
            )
        );

        this.openingHoursRepository = new OpeningHoursRepository();

        this.sequelizeModel.hasMany(this.openingHoursRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "institution_id",
        });
    }

    public async saveInstitutions(transformedData: IMedicalInstitutionsTransformation) {
        const updatedAt = new Date().toISOString();

        const connection = PostgresConnector.getConnection();
        const t = await connection.transaction();
        try {
            const ids = await this.saveInstitutionsData(transformedData.institutions, t);
            await this.saveOpeningHoursData(transformedData.openingHours, ids, updatedAt, t);

            await t.commit();
        } catch (err) {
            await t.rollback();
            throw err;
        }
    }

    private async saveInstitutionsData(institutions: IInstitution[], t: Transaction): Promise<string[]> {
        await this.validate(institutions);
        await this.sequelizeModel.bulkCreate<InstitutionsModel>(institutions, {
            updateOnDuplicate: InstitutionsModel.updateAttributes,
            transaction: t,
        });
        return institutions.map((ins) => ins.id);
    }

    private async saveOpeningHoursData(
        hours: Array<Omit<IOpeningHours, "id">>,
        ids: string[],
        updatedAt: string,
        t: Transaction
    ) {
        await this.openingHoursRepository.validate(hours);
        await this.openingHoursRepository["sequelizeModel"].bulkCreate(hours, {
            updateOnDuplicate: OpeningHoursModel.updateAttributes,
            transaction: t,
        });
        await this.openingHoursRepository["sequelizeModel"].destroy({
            where: { institution_id: { [Op.in]: ids }, updated_at: { [Op.lt]: updatedAt } },
            transaction: t,
        });
    }
}
