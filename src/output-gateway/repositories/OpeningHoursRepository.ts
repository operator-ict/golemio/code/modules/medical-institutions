import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { MedicalInstitutions } from "#sch";
import { OpeningHoursModel } from "#sch/models";

export class OpeningHoursRepository extends SequelizeModel {
    constructor() {
        super(
            "OpeningHoursRepository",
            MedicalInstitutions.definitions.medicalInstitutionsOpeningHours.pgTableName,
            OpeningHoursModel.attributeModel,
            { schema: MedicalInstitutions.pgSchema }
        );
    }

    public GetAll = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };

    public GetOne = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };
}
