import Sequelize from "@golemio/core/dist/shared/sequelize";
import { SequelizeModel } from "@golemio/core/dist/output-gateway";
import { IGeoJsonModel } from "@golemio/core/dist/output-gateway/models/interfaces/IGeoJsonModel";
import {
    buildGeojsonFeatureCollection,
    buildGeojsonFeatureLatLng,
    IGeoJsonAllFilterParameters,
    IGeoJSONFeature,
    IGeoJSONFeatureCollection,
} from "@golemio/core/dist/output-gateway";
import { MedicalInstitutions } from "#sch";
import { InstitutionsModel } from "#sch/models";
import { OpeningHoursRepository } from "#og/repositories/OpeningHoursRepository";

interface IMedicalInstitutionTypesOutput {
    pharmacies: string[];
    health_care: string[];
}

export class MedicalInstitutionsRepository extends SequelizeModel implements IGeoJsonModel {
    private readonly openingHoursRepository: OpeningHoursRepository;

    constructor() {
        super(
            "MedicalInstitutionsRepository",
            MedicalInstitutions.definitions.medicalInstitutions.pgTableName,
            InstitutionsModel.attributeModel,
            { schema: MedicalInstitutions.pgSchema }
        );

        this.openingHoursRepository = new OpeningHoursRepository();

        this.sequelizeModel.hasMany(this.openingHoursRepository["sequelizeModel"], {
            sourceKey: "id",
            foreignKey: "institution_id",
        });
    }

    public IsPrimaryIdNumber(idKey: string): Promise<boolean> {
        return Promise.resolve(false);
    }

    public PrimaryIdentifierSelection(id: string): object {
        return { id };
    }

    public GetProperties = async (): Promise<any> => {
        throw new Error("Method not implemented.");
    };

    public GetAll = async (
        options: IGeoJsonAllFilterParameters & { additionalFilters?: { group: string } } = {}
    ): Promise<IGeoJSONFeatureCollection> => {
        const whereAttributes: Sequelize.WhereOptions = {};
        const order: Sequelize.Order = [];

        if (options.districts && options.districts.length > 0) {
            whereAttributes.district = {
                [Sequelize.Op.in]: options.districts,
            };
        }

        if (options.additionalFilters?.group) {
            whereAttributes.type = {
                group: {
                    [Sequelize.Op.eq]: options.additionalFilters.group,
                },
            };
        }

        if (options.updatedSince) {
            whereAttributes.updated_at = {
                [Sequelize.Op.gt]: options?.updatedSince,
            };
        }

        if (options.lat && options.lng) {
            if (options.range) {
                whereAttributes.range = Sequelize.where(
                    Sequelize.fn(
                        "ST_DWithin",
                        Sequelize.col("geometry"),
                        Sequelize.cast(
                            Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                            "geography"
                        ),
                        options.range,
                        true
                    ),
                    "true"
                );
            }

            order.push(
                Sequelize.fn(
                    "ST_Distance",
                    Sequelize.col("geometry"),
                    Sequelize.cast(
                        Sequelize.fn("ST_SetSRID", Sequelize.fn("ST_MakePoint", options.lng, options.lat), 4326),
                        "geography"
                    )
                )
            );
        }

        order.push([{ model: this.openingHoursRepository["sequelizeModel"], as: "opening_hours" }, "id", "asc"]);

        const result = await this.sequelizeModel.findAll<InstitutionsModel>({
            attributes: {
                include: ["updated_at"],
            },
            include: [
                {
                    model: this.openingHoursRepository["sequelizeModel"],
                    required: false,
                    as: "opening_hours",
                    attributes: {
                        exclude: ["id", "institution_id"],
                    },
                },
            ],
            where: whereAttributes,
            limit: options?.limit,
            offset: Number.isInteger(options?.offset) ? options?.offset : undefined,
            order,
        });

        return buildGeojsonFeatureCollection(
            result.map((record: InstitutionsModel) => {
                return this.formatOutput(record);
            })
        );
    };

    public GetOne = async (id: string): Promise<IGeoJSONFeature | undefined> => {
        const result = await this.sequelizeModel.findOne<InstitutionsModel>({
            attributes: {
                include: ["updated_at"],
            },
            where: { id },
            include: [
                {
                    model: this.openingHoursRepository["sequelizeModel"],
                    required: false,
                    as: "opening_hours",
                    attributes: {
                        exclude: ["id", "institution_id"],
                    },
                },
            ],
        });

        return result ? this.formatOutput(result) : undefined;
    };

    public GetTypes = async (): Promise<IMedicalInstitutionTypesOutput> => {
        return {
            health_care: [
                "Fakultní nemocnice",
                "Nemocnice",
                "Ostatní ambulantní zařízení",
                "Ostatní zdravotnická zařízení",
                "Zdravotní záchranná služba",
                "Zdravotnické středisko",
            ],
            pharmacies: [
                "Nemocniční lékárna s odbornými pracovišti",
                "Lékárna",
                "Lékárna s odbornými pracovišti",
                "Výdejna",
                "Vojenská lékárna",
                "Vojenská lékárna s OOVL",
                "Nemocniční lékárna",
                "Lékárna s odbornými pracovišti s OOVL",
                "Nemocniční lékárna s OOVL",
                "Nemocniční lékárna s odbor. pracovišti s OOVL",
                "Odloučené oddělení výdeje léčivých přípravků",
                "Lékárna s OOVL",
                "Lékárna s odborným pracovištěm, která zásobuje lůžková zdravotnická zařízení",
                "Lékárna s odborným pracovištěm, která zásobuje lůžková zdravotnická zařízení s OOVL",
                "Lékárna zásobujicí zařízení ústavní péče",
                "Lékárna zásobujicí zařízení ústavní péče s OOVL",
            ],
        };
    };

    private formatOutput = (record: InstitutionsModel): IGeoJSONFeature | undefined => {
        const { geometry, ...rest } = record.get({ plain: true });
        return buildGeojsonFeatureLatLng(rest, geometry.coordinates[0], geometry.coordinates[1]);
    };
}
