import { MedicalInstitutionsRepository } from "#og/repositories/MedicalInstitutionsRepository";
import { parseCoordinates } from "@golemio/core/dist/output-gateway/Geo";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { GeoJsonRouter } from "@golemio/core/dist/output-gateway/routes";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { query } from "@golemio/core/dist/shared/express-validator";

const CACHE_MAX_AGE = 12 * 60 * 60;
const CACHE_STALE_WHILE_REVALIDATE = 60 * 60;

export class MedicalInstitutionsRouter extends GeoJsonRouter {
    constructor() {
        super(new MedicalInstitutionsRepository());
        this.router.get(
            "/types",
            checkErrors,
            this.cacheHeaderMiddleware.getMiddleware(CACHE_MAX_AGE, CACHE_STALE_WHILE_REVALIDATE),
            this.GetTypes
        );
        this.initRoutes({ maxAge: CACHE_MAX_AGE, staleWhileRevalidate: CACHE_STALE_WHILE_REVALIDATE });
        this.router.get(
            "/",
            [query("group").optional().isIn(["pharmacies", "health_care"]).not().isArray()],
            this.standardParams,
            pagination,
            checkErrors,
            paginationLimitMiddleware("MedicalInstitutionsRouter"),
            this.cacheHeaderMiddleware.getMiddleware(CACHE_MAX_AGE, CACHE_STALE_WHILE_REVALIDATE),
            this.GetAll
        );
    }

    public GetAll = async (req: Request, res: Response, next: NextFunction) => {
        // Parsing parameters
        let ids: number[] = req.query.ids as unknown as number[];
        let districts: string[] = req.query.districts as string[];
        let additionalFilters = { group: req.query.group };

        if (districts) {
            districts = this.ConvertToArray(districts);
        }
        if (ids) {
            ids = this.ConvertToArray(ids);
        }
        try {
            const coords = await parseCoordinates(req.query.latlng as string, req.query.range as string);

            const data = await this.model.GetAll({
                additionalFilters,
                districts,
                ids,
                lat: coords.lat,
                limit: Number(req.query.limit) || undefined,
                lng: coords.lng,
                offset: Number(req.query.offset) || undefined,
                range: coords.range,
                updatedSince: req.query.updatedSince as string,
            });

            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };

    public GetTypes = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await (this.model as MedicalInstitutionsRepository).GetTypes();
            res.status(200).send(data);
        } catch (err) {
            next(err);
        }
    };
}

const medicalInstitutionsRouter: Router = new MedicalInstitutionsRouter().router;

export { medicalInstitutionsRouter };
