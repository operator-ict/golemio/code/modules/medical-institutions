import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { MedicalInstitutionsRepository } from "#og/repositories/MedicalInstitutionsRepository";
import { IInstitution } from "#sch/Institutions";

chai.use(chaiAsPromised);

describe("MedicalInstitutionsRepository", () => {
    let medicalInstitutionRepository: MedicalInstitutionsRepository;

    before(async () => {
        medicalInstitutionRepository = new MedicalInstitutionsRepository();
    });

    it("should instantiate", () => {
        expect(medicalInstitutionRepository).not.to.be.undefined;
    });

    it("should return all items", async () => {
        const result = await medicalInstitutionRepository.GetAll();
        expect(result).to.be.an.instanceOf(Object);
        expect(result.features).to.be.an.instanceOf(Array);
        expect(result.features).to.have.length(4);
    });

    it("should return single item", async () => {
        const id: string = "00200379227-adamova-lekarna";
        const result = await medicalInstitutionRepository.GetOne(id);
        expect(result).not.to.be.empty;
        expect(result).to.be.an.instanceOf(Object);
        expect(result!.properties).to.have.property("id", id);
        expect(result!.properties).to.have.property("opening_hours");
        expect((result!.properties as IInstitution).opening_hours).to.have.length(6);
    });
});
