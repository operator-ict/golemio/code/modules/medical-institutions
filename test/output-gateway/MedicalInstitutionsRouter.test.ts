import request from "supertest";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { medicalInstitutionsRouter } from "#og/MedicalInstitutionsRouter";
import { outputDataFixture } from "./data/medicalinstitutions-output";

chai.use(chaiAsPromised);

describe("MedicalInstitutions Router", () => {
    const app = express();

    before(async () => {
        app.use("/medicalinstitutions", medicalInstitutionsRouter);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond with json to GET /medicalinstitutions", (done) => {
        request(app)
            .get("/medicalinstitutions")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body.features.length).to.eql(4);
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /medicalinstitutions?group=health_care", (done) => {
        request(app)
            .get("/medicalinstitutions?group=health_care")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .then((response) => {
                expect(response.body.features.length).to.eql(3);
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /medicalinstitutions/:id", (done) => {
        request(app)
            .get("/medicalinstitutions/173473-carpe-diem-bohemia-sro")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .expect(200)
            .then((response) => {
                expect(response.body).to.eql(outputDataFixture);
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond with json to GET /medicalinstitutions/types", (done) => {
        request(app)
            .get("/medicalinstitutions/types")
            .set("Accept", "application/json")
            .expect("Content-Type", /json/)
            .then((response) => {
                expect(response.body.pharmacies.length).to.eql(16);
                expect(response.body.health_care.length).to.eql(6);
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond to GET /medicalinstitutions with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/medicalinstitutions")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond to GET /medicalinstitutions/:id with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/medicalinstitutions/173473-carpe-diem-bohemia-sro")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });

    it("should respond to GET /medicalinstitutions/types with the correct Cache-Control headers", (done) => {
        request(app)
            .get("/medicalinstitutions/types")
            .set("Accept", "application/json")
            .expect(200)
            .then((response) => {
                expect(response.headers["cache-control"]).to.equal("public, s-maxage=43200, stale-while-revalidate=3600");
                done();
            })
            .catch((err) => done(err));
    });
});
