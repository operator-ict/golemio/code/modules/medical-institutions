export const outputDataFixture = {
    geometry: {
        coordinates: [14.371338152721, 50.070569361243],
        type: "Point",
    },
    properties: {
        id: "173473-carpe-diem-bohemia-sro",
        name: "CARPE DIEM BOHEMIA s.r.o.",
        institution_code: "173473",
        pharmacy_code: null,
        address: {
            address_country: "Česko",
            address_formatted: "U dvou srpů 2024/2, 15000 Praha 5, Česko",
            address_locality: "Praha 5",
            postal_code: "15000",
            street_address: "U dvou srpů 2024/2",
        },
        district: null,
        web: ["http://www.carpebohemia.cz"],
        email: ["reditel@carpebohemia.cz"],
        telephone: [],
        type: {
            description: "Ostatní ambulantní zařízení",
            group: "health_care",
            id: "ZS-OAZ",
        },
        updated_at: "2022-11-21T07:20:08.528Z",
        opening_hours: [],
    },
    type: "Feature",
};
