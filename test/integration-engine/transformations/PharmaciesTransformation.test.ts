import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { readFileSync } from "fs";
import { PharmaciesTransformation } from "#ie/transformations";

chai.use(chaiAsPromised);

describe("PharmaciesTransformation", () => {
    let transformation: PharmaciesTransformation;
    let testSourceData: any[];

    beforeEach(async () => {
        transformation = new PharmaciesTransformation();
        testSourceData = JSON.parse(readFileSync(__dirname + "/../data/pharmacies-files.json", "utf8"));
    });

    it("should has name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("PharmaciesFilesDatasource");
    });

    it("should has transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        testSourceData[0].data = readFileSync(__dirname + "/../data/medicalinstitutions/lekarny_prac_doba.csv");
        testSourceData[1].data = readFileSync(__dirname + "/../data/medicalinstitutions/lekarny_seznam.csv");
        testSourceData[2].data = readFileSync(__dirname + "/../data/medicalinstitutions/lekarny_typ.csv");
        const data = await transformation.transform(testSourceData);
        expect(data.institutions.length).to.equal(4);
        expect(data.openingHours.length).to.equal(22);
        for (const institution of data.institutions) {
            expect(Object.keys(institution).length).to.equal(11);
            expect(institution).to.have.property("id");
            expect(institution).to.have.property("name");
            expect(institution).to.have.property("pharmacy_code");
            expect(institution).to.have.property("institution_code");
            expect(institution).to.have.property("geometry");
            expect(institution).to.have.property("address");
            expect(institution).to.have.property("district");
            expect(institution).to.have.property("telephone");
            expect(institution).to.have.property("web");
            expect(institution).to.have.property("email");
            expect(institution).to.have.property("type");
            expect(institution.type).to.have.property("id");
            expect(institution.type).to.have.property("description");
            expect(institution.type).to.have.property("group");
        }

        for (const hour of data.openingHours) {
            expect(Object.keys(hour).length).to.equal(4);
            expect(hour).to.have.property("institution_id");
            expect(hour).to.have.property("day_of_week");
            expect(hour).to.have.property("opens");
            expect(hour).to.have.property("closes");
        }
    });
});
