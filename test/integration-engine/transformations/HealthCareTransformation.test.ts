import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { readFileSync } from "fs";
import { transformedDataFixture } from "../data/healthcare-transformation";
import { HealthCareTransformation } from "#ie/transformations";

chai.use(chaiAsPromised);

describe("HealthCareTransformation", () => {
    let transformation: HealthCareTransformation;
    let testSourceData: any[];

    beforeEach(async () => {
        transformation = new HealthCareTransformation();
        testSourceData = JSON.parse(readFileSync(__dirname + "/../data/healthcare-datasource.json", "utf8"));
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("HealthCareDatasource");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should correctly transform element", async () => {
        const data = await transformation["transformElement"](testSourceData[0]);
        expect(data).to.deep.equal(transformedDataFixture);
    });

    it("should correctly transform collection", async () => {
        const data = await transformation.transform(testSourceData);
        expect(data.length).to.equal(4);
        for (const item of data) {
            expect(Object.keys(item).length).to.equal(11);
            expect(item).to.have.property("id");
            expect(item).to.have.property("name");
            expect(item).to.have.property("pharmacy_code");
            expect(item).to.have.property("institution_code");
            expect(item).to.have.property("geometry");
            expect(item).to.have.property("address");
            expect(item).to.have.property("district");
            expect(item).to.have.property("telephone");
            expect(item).to.have.property("web");
            expect(item).to.have.property("email");
            expect(item).to.have.property("type");
            expect(item.type).to.have.property("id");
            expect(item.type).to.have.property("description");
            expect(item.type).to.have.property("group");
        }
    });
});
