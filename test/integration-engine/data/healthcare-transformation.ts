import { Point } from "@golemio/core/dist/shared/geojson";

export const transformedDataFixture = {
    id: "132127-ing-richard-tomanek-vydejna-zdravotnickych-prostredku",
    name: "Ing. Richard Tománek, Výdejna zdravotnických prostředků",
    pharmacy_code: null,
    institution_code: "132127",
    geometry: {
        coordinates: [14.514849240911, 50.025628051691],
        type: "Point",
    },
    address: {
        address_country: "Česko",
        address_formatted: "Křejpského 1752/2, 14900 Praha 11, Česko",
        address_locality: "Praha 11",
        postal_code: "14900",
        street_address: "Křejpského 1752/2",
    },
    district: null,
    telephone: ["773929202", "773929203"],
    web: ["www.zpflorence.cz"],
    email: ["info@zpflorence.cz"],
    type: {
        description: "Výdejna zdravotnických prostředků",
        group: "health_care",
        id: "ZS-VZP",
    },
};
