import { createSandbox, SinonSandbox } from "sinon";
import { expect } from "chai";
import { PostgresConnector, RedisConnector } from "@golemio/core/dist/integration-engine";
import { MedicalInstitutionsWorker } from "#ie/workers/MedicalInstitutionsWorker";

describe("MedicalInstitutionsWorker", () => {
    let sandbox: SinonSandbox;
    let worker: MedicalInstitutionsWorker;

    beforeEach(() => {
        sandbox = createSandbox();
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );
        sandbox.stub(RedisConnector, "getConnection");

        worker = new MedicalInstitutionsWorker();
    });

    afterEach(() => {
        sandbox.restore();
    });

    describe("getQueuePrefix", () => {
        it("should have correct queue prefix set", () => {
            const result = worker["getQueuePrefix"]();
            expect(result).to.contain(".medicalinstitutions");
        });
    });

    describe("getQueueDefinition", () => {
        it("should return correct queue definition", () => {
            const result = worker.getQueueDefinition();
            expect(result.name).to.equal("MedicalInstitutions");
            expect(result.queues.length).to.equal(2);
        });
    });

    describe("registerTask", () => {
        it("should have two tasks registered", () => {
            expect(worker["queues"].length).to.equal(2);

            expect(worker["queues"][0].name).to.equal("refreshDataInDB");
            expect(worker["queues"][1].name).to.equal("updateGeoAndDistrict");

            expect(worker["queues"][0].options.messageTtl).to.equal(25 * 24 * 60 * 60 * 1000);
            expect(worker["queues"][1].options.messageTtl).to.equal(25 * 24 * 60 * 60 * 1000);
        });
    });
});
