import { createSandbox, SinonSandbox, SinonSpy } from "sinon";
import { GeocodeApi, PostgresConnector } from "@golemio/core/dist/integration-engine";
import { UpdateGeoAndDistrictTask } from "#ie/workers/tasks";
import { IUpdateDistrictInput } from "#ie/workers/schemas/UpdateDistrictSchema";
import { InstitutionsModel } from "#sch/models";

describe("UpdateGeoAndDistrictTask", () => {
    let sandbox: SinonSandbox;
    let task: UpdateGeoAndDistrictTask;
    let msg: IUpdateDistrictInput;
    let institutionsModel: InstitutionsModel;

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
            })
        );

        msg = { id: "136562-czolko", geometry: { type: "Point", coordinates: [0, 0] }, district: null };
        institutionsModel = {
            id: "136562-czolko",
            geometry: { coordinates: [0, 0], type: "Point" },
            district: undefined,
            address: {
                street_address: "street",
                address_locality: "locality",
            },
        } as unknown as InstitutionsModel;

        task = new UpdateGeoAndDistrictTask("test.medicalinstitutions");

        sandbox.stub(task["repository"], "findOne").callsFake(() => Promise.resolve(institutionsModel));
        sandbox.stub(task["repository"], "save");

        sandbox.stub(GeocodeApi, "getGeoByAddress").callsFake(() => Promise.resolve([14.33, 52.22]));
        sandbox.stub(task["cityDistrictsModel"], "getDistrict").callsFake(() => Promise.resolve("praha-2"));
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by updateDistrict method", async () => {
        await task["execute"](msg);
        sandbox.assert.calledOnce(task["repository"].findOne as SinonSpy);
        sandbox.assert.calledWith(task["repository"].findOne as SinonSpy, { where: { id: msg.id }, raw: true });

        sandbox.assert.calledOnce(task["cityDistrictsModel"].getDistrict as SinonSpy);
        sandbox.assert.calledOnce(task["repository"].save as SinonSpy);
        sandbox.assert.calledWith(task["repository"].save as SinonSpy, [
            {
                ...institutionsModel,
                geometry: { type: "Point", coordinates: [14.33, 52.22] },
                district: "praha-2",
            },
        ]);

        sandbox.assert.callOrder(
            task["repository"].findOne as SinonSpy,
            task["cityDistrictsModel"].getDistrict as SinonSpy,
            task["repository"].save as SinonSpy
        );
    });
});
