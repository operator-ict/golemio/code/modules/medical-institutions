import { createSandbox, SinonSandbox, SinonSpy } from "sinon";
import { PostgresConnector, QueueManager, RedisConnector } from "@golemio/core/dist/integration-engine";
import { RefreshDataInDBTask } from "#ie/workers/tasks";
import { IHealthCareInput } from "#sch/datasources";
import { IMedicalInstitutionsTransformation } from "#ie/transformations";
import { IInstitution } from "#sch/Institutions";
import { IOpeningHours } from "#sch/OpeningHours";

describe("RefreshDataInDBTask", () => {
    let sandbox: SinonSandbox;
    let task: RefreshDataInDBTask;
    let testData: IHealthCareInput[];
    let testTransformedDataPharmacy: IMedicalInstitutionsTransformation;
    let testTransformedDataHealthCare: IInstitution[];
    let testTransformedData: IMedicalInstitutionsTransformation;

    beforeEach(() => {
        sandbox = createSandbox({ useFakeTimers: true });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().returns({ belongsTo: sandbox.stub(), hasOne: sandbox.stub(), hasMany: sandbox.stub() }),
                transaction: sandbox.stub().returns({ commit: sandbox.stub() }),
            })
        );
        sandbox.stub(RedisConnector, "getConnection").callsFake(() =>
            Object.assign({
                hget: sandbox.stub(),
            })
        );

        task = new RefreshDataInDBTask("test.medicalinstitutions");

        testData = [
            {
                ZdravotnickeZarizeniId: "136562",
                NazevZarizeni: "Czolko, s.r.o.",
                Lat: "50.105816367522",
                Lng: "14.389110429241",
            },
        ] as IHealthCareInput[];

        testTransformedDataPharmacy = {
            institutions: [{ id: "136562-czolko", geometry: { coordinates: [0, 0], type: "Point" } } as IInstitution],
            openingHours: [
                { institution_id: "136562-czolko", opens: "09:00", closes: "17:30", day_of_week: "Monday" } as IOpeningHours,
            ],
        };

        testTransformedDataHealthCare = [
            { id: "1365645-lekarna", geometry: { coordinates: [0, 0], type: "Point" } } as IInstitution,
        ];

        testTransformedData = {
            institutions: testTransformedDataHealthCare.concat(testTransformedDataPharmacy.institutions),
            openingHours: testTransformedDataPharmacy.openingHours,
        };

        sandbox.stub(task["healthCareDataSource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox.stub(task["pharmaciesDataSource"], "getAll").callsFake(() => Promise.resolve(testData));
        sandbox
            .stub(task["healthCareTransformation"], "transform")
            .callsFake(() => Promise.resolve(testTransformedDataHealthCare));
        sandbox.stub(task["pharmaciesTransformation"], "transform").callsFake(() => Promise.resolve(testTransformedDataPharmacy));

        sandbox.stub(task["repository"], "saveInstitutions");
        sandbox.stub(QueueManager, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call the correct methods by refreshDataInDB method", async () => {
        await task["execute"]();
        sandbox.assert.calledOnce(task["healthCareDataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["pharmaciesDataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(task["healthCareTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(task["healthCareTransformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(task["pharmaciesTransformation"].transform as SinonSpy);
        sandbox.assert.calledWith(task["pharmaciesTransformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(task["repository"].saveInstitutions as SinonSpy);
        sandbox.assert.calledWith(task["repository"].saveInstitutions as SinonSpy, testTransformedData);
        sandbox.assert.callCount(QueueManager["sendMessageToExchange"] as SinonSpy, 2);

        for (const data of testTransformedData.institutions) {
            sandbox.assert.calledWith(
                QueueManager["sendMessageToExchange"] as SinonSpy,
                "test.medicalinstitutions",
                "updateGeoAndDistrict",
                { id: data.id, geometry: { coordinates: [0, 0], type: "Point" }, district: data.district }
            );
        }

        sandbox.assert.callOrder(
            task["healthCareDataSource"].getAll as SinonSpy,
            task["healthCareTransformation"].transform as SinonSpy,
            task["pharmaciesDataSource"].getAll as SinonSpy,
            task["pharmaciesTransformation"].transform as SinonSpy,
            task["repository"].saveInstitutions as SinonSpy,
            QueueManager["sendMessageToExchange"] as SinonSpy
        );
    });
});
