# Implementační dokumentace modulu _medical-institutions_

## Záměr

Modul slouží k ukládání a poskytování informací o zdravotníckych zařízeních v Praze.

## Vstupní data

**Dostupní poskytovatelé**:

-   https://opendata.sukl.cz
-   https://opendata.mzcr.cz

### Data aktivně stahujeme

#### _open data SUKL_

-   zdroj dat
    -   url: [config.datasources.MedicalInstitutionsPharmacies](https://opendata.sukl.cz/soubory/SODERECEPT/LEKARNYAKTUALNI.zip)
-   formát dat
    -   protokol: http
    -   datový typ: zip csv ("lekarny_prac_doba.csv", "lekarny_seznam.csv", "lekarny_typ.csv")
    -   validační schéma: [pharmaciesFilesJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/medical-institutions/-/blob/development/src/schema-definitions/datasources/PharmaciesFilesJsonSchema.ts)
    -   příklad [vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/medical-institutions/-/blob/development/test/integration-engine/data/medicalinstitutions/)

#### _open data MZCR_

-   zdroj dat
    -   url: [config.datasources.MedicalInstitutionsHealthCare](https://opendata.mzcr.cz/data/nrpzs/narodni-registr-poskytovatelu-zdravotnich-sluzeb.csv)
-   formát dat

    -   protokol: http
    -   datový typ: csv
    -   validační schéma: [healthCareJsonSchema](https://gitlab.com/operator-ict/golemio/code/modules/medical-institutions/-/blob/development/src/schema-definitions/datasources/HealthCareJsonSchema.ts)
    -   příklad [vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/medical-institutions/-/blob/development/test/integration-engine/data/healthcare-datasource.json)

-   frekvence stahování
    -   cron definice:
        -   cron.dataplatform.medicalInstitutions.refreshDataInDB
            -   rabin `0 31 7 * * *`
            -   prod `0 25 2 1 * *`
-   název rabbitmq fronty
    -   dataplatform.medicalinstitutions.refreshDataInDB

## Zpracování dat / transformace

Při transformaci data obohacujeme o atribut `district`, která vychází z polohy daného zařížení a získává se metodou `cityDistrictsModel.getDistrict` v modulu city-district.
Data `SUKL` obohacujeme o geometrii z adresy z `GeocodeApi` metodou `getGeoByAddress`

### _MedicalInstitutionsWorker_

#### _task: RefreshDataInDBTask_

-   vstupní rabbitmq fronta
    -   název: dataplatform.medicalinstitutions.refreshDataInDB
    -   bez parametrů
-   závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    -   název: dataplatform.medicalinstitutions.updateGeoAndDistrict
    -   parametry: `{ id: institution.id; geometry: institution.geometry; district: institution.district }`
-   datové zdroje
    -   dataSource SUKL & MZCR
-   transformace
    -   [HealthCareTransformation](https://gitlab.com/operator-ict/golemio/code/modules/medical-institutions/-/blob/development/src/integration-engine/transformations/HealthCareTransformation.ts)
    -   [PharmaciesTransformation](https://gitlab.com/operator-ict/golemio/code/modules/medical-institutions/-/blob/development/src/integration-engine/transformations/PharmaciesTransformation.ts)
-   data modely
    -   institutionsModel -> (schéma medical_institutions) `medical_institutions`
    -   openingHoursModel -> (schéma medical_institutions) `opening_hours`

#### _task: UpdateGeoAndDistrict_

-   vstupní rabbitmq fronta
    -   název: dataplatform.medicalinstitutions.updateGeoAndDistrict
    -   parametry: `{ id: institution.id; geometry: institution.geometry; district: institution.district }`
-   datové zdroje
    -   `city-districts` module
    -   `GeocodeApi`
-   data modely
    -   institutionsModel -> (schéma medical_institutions) `institution.district`

## Uložení dat

-   typ databáze
    -   PSQL
-   databázové schéma
    -   ![medicalinstitutions er diagram](./assets/medicalinstitutions_erd.png)
-   retence dat
    -   `institutionsModel` update
    -   `openingHoursModel` replace

## Output API

`MedicalInstitutionsRouter` implementuje `GeoJsonRouter`.

### Obecné

-   OpenAPI v3 dokumentace
    -   [openapi.yaml](https://gitlab.com/operator-ict/golemio/code/modules/medical-institutions/-/blob/development/docs/openapi.yaml)
-   api je veřejné
-   postman kolekce
    -   TBD
-   Asyncapi dokumentace RabbitMQ front
    -   [AsyncAPI](./asyncapi.yaml)

#### _/medicalinstitutions_

-   zdrojové tabulky
    -   `institutions`, `opening_hours`,
-   dodatečná transformace: Feature collection

#### _/medicalinstitutions/:id_

-   zdrojové tabulky
    -   `institutions`, `opening_hours`,
-   dodatečná transformace: Feature

#### _/medicalinstitutions/types_

-   statické data
