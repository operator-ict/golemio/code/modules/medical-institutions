openapi: 3.0.3

info:
    title: 🏥 Medical Institutions
    description: Locations and Description of Medical Institutions
    version: 1.0.0
    contact:
        name: Golemio Prague Data Platform
        email: golemio@operatorict.cz
        url: https://golemio.cz

servers:
    - url: https://api.golemio.cz
      description: Main (production) server
    - url: https://rabin.golemio.cz
      description: Test (development) server

tags:
    - name: 🏥 Medical Institutions (v2)
      description: <img src="https://img.shields.io/badge/opendata-available-green" alt="golemioapi-opendata-badge" /> 💡 Locations and Description of Medical Institutions

paths:
    /v2/medicalinstitutions:
        get:
            summary: GET All Medical Institutions
            operationId: GETAllMedicalInstitutions
            description: ""
            tags:
                - 🏥 Medical Institutions (v2)
            parameters:
                - name: latlng
                  in: query
                  description: Sorting by location (Latitude and Longitude separated by comma,
                      latitude first).
                  required: false
                  example: 50.124935,14.457204
                  schema:
                      type: string
                - name: range
                  in: query
                  description: Filter by distance from latlng in meters (range query). Depends on
                      the latlng parameter.
                  required: false
                  example: 5000
                  schema:
                      type: number
                - name: districts
                  in: query
                  description: Filter by Prague city districts (slug) separated by comma.
                  required: false
                  example: ['praha-4']
                  schema:
                      type: array
                      items: {}
                - name: group
                  in: query
                  description: Filter by group of types of the medical institution.
                  required: false
                  example: pharmacies
                  schema:
                    type: string
                    enum: [pharmacies, health_care]
                - name: limit
                  in: query
                  description: Limits number of retrieved items. The maximum is 10000 (default
                      value).
                  required: false
                  example: 10
                  schema:
                      type: number
                - name: offset
                  in: query
                  description: Number of the first items that are skipped.
                  required: false
                  example: 0
                  schema:
                      type: number
                - name: updatedSince
                  in: query
                  description: Filters all results with older updated_at than this parameter
                  required: false
                  example: 2019-05-18T07:38:37.000Z
                  schema:
                      type: string
            responses:
                "200":
                    description: OK
                    headers:
                        Cache-Control:
                            description: Cache control directive for caching proxies
                            schema:
                                type: string
                                example: public, s-maxage=43200, stale-while-revalidate=3600
                    content:
                        application/json; charset=utf-8:
                            schema:
                                type: object
                                properties:
                                    type:
                                        type: string
                                        example: FeatureCollection
                                    features:
                                        type: array
                                        items:
                                            $ref: "#/components/schemas/MedicalInstitution"
                                required:
                                    - type
                                    - features

                "401":
                    $ref: "#/components/responses/UnauthorizedError"
                "403":
                    description: Forbidden
                    headers: {}
                    content:
                        application/json; charset=utf-8:
                            schema:
                                type: object
                                properties:
                                    error_message:
                                        type: string
                                    error_status:
                                        type: number
                                required:
                                    - error_message
                                    - error_status
                            examples:
                                response:
                                    value:
                                        error_message: Forbidden
                                        error_status: 403

    /v2/medicalinstitutions/{id}:
        get:
            summary: GET Medical Institution
            operationId: GETMedicalInstitution
            description: ""
            tags:
                - 🏥 Medical Institutions (v2)
            parameters:
                - name: id
                  in: path
                  description: Identifier of the Medical Institution.
                  required: true
                  example: 252671-fakultni-nemocnice-v-motole
                  schema:
                      type: string
            responses:
                "200":
                    description: OK
                    headers:
                        Cache-Control:
                            description: Cache control directive for caching proxies
                            schema:
                                type: string
                                example: public, s-maxage=43200, stale-while-revalidate=3600
                    content:
                        application/json; charset=utf-8:
                            schema:
                                $ref: "#/components/schemas/MedicalInstitution"
                "401":
                    $ref: "#/components/responses/UnauthorizedError"
                "403":
                    description: Forbidden
                    headers: {}
                    content:
                        application/json; charset=utf-8:
                            schema:
                                type: object
                                properties:
                                    error_message:
                                        type: string
                                    error_status:
                                        type: number
                                required:
                                    - error_message
                                    - error_status
                            examples:
                                response:
                                    value:
                                        error_message: Forbidden
                                        error_status: 403
                "404":
                    description: Not Found
                    headers: {}
                    content:
                        application/json; charset=utf-8:
                            schema:
                                type: object
                                properties:
                                    error_message:
                                        type: string
                                    error_status:
                                        type: number
                                required:
                                    - error_message
                                    - error_status
                            examples:
                                response:
                                    value:
                                        error_message: Not Found
                                        error_status: 404

    /v2/medicalinstitutions/types:
        get:
            summary: GET All Medical Institution Types
            operationId: GETAllMedicalInstitutionTypes
            description: ""
            tags:
                - 🏥 Medical Institutions (v2)
            responses:
                "200":
                    description: OK
                    headers:
                        Cache-Control:
                            description: Cache control directive for caching proxies
                            schema:
                                type: string
                                example: public, s-maxage=43200, stale-while-revalidate=3600
                    content:
                        application/json; charset=utf-8:
                            schema:
                                $ref: "#/components/schemas/MedicalInstitutionType"
                "401":
                    $ref: "#/components/responses/UnauthorizedError"
                "403":
                    description: Forbidden
                    headers: { }
                    content:
                        application/json; charset=utf-8:
                            schema:
                                type: object
                                properties:
                                    error_message:
                                        type: string
                                    error_status:
                                        type: number
                                required:
                                    - error_message
                                    - error_status
                            examples:
                                response:
                                    value:
                                        error_message: Forbidden
                                        error_status: 403
                "404":
                    description: Not Found
                    headers: { }
                    content:
                        application/json; charset=utf-8:
                            schema:
                                type: object
                                properties:
                                    error_message:
                                        type: string
                                    error_status:
                                        type: number
                                required:
                                    - error_message
                                    - error_status
                            examples:
                                response:
                                    value:
                                        error_message: Not Found
                                        error_status: 404

components:
    responses:
        UnauthorizedError:
            description: API key is missing or invalid
            headers:
                WWW_Authenticate:
                    schema:
                        type: string
    schemas:
        MedicalInstitution:
            type: object
            properties:
                geometry:
                    type: object
                    properties:
                        type:
                            type: string
                            example: Point
                        coordinates:
                            type: array
                            items:
                                type: number
                            example:
                                - 14.392211
                                - 50.094008
                properties:
                    type: object
                    properties:
                        address:
                            type: object
                            properties:
                                address_formatted:
                                    type: string
                                    example: Dělnická 213/10, 17000 Praha-Holešovice, Česko
                                street_address:
                                    type: string
                                    example: Dělnická 213/10
                                postal_code:
                                    type: string
                                    example: 17000
                                address_locality:
                                    type: string
                                    example: Praha
                                address_region:
                                    type: string
                                    example: Holešovice
                                address_country:
                                    type: string
                                    example: Česko
                        district:
                            type: string
                            example: praha-7
                        email:
                            type: array
                            items:
                                type: string
                            example:
                                - praha@benu.cz
                        id:
                            type: string
                            example: 04995142000-benu-lekarna
                        name:
                            type: string
                            example: BENU Lékárna
                        institution_code:
                            type: string
                            example: 04995142000
                        pharmacy_code:
                            type: string
                            example: 04996100
                        opening_hours:
                            type: array
                            items:
                                type: object
                                properties:
                                    closes:
                                        type: string
                                        example: 12:00
                                    day_of_week:
                                        type: string
                                        example: Monday
                                    opens:
                                        type: string
                                        example: 09:00
                        telephone:
                            type: array
                            items:
                                type: string
                            example:
                                - +420 770 123 456
                        updated_at:
                            type: string
                            example: 2019-05-18T07:38:37.000Z
                        web:
                            type: array
                            items:
                                type: string
                            example:
                                - http://www.benu.cz
                        type:
                            type: object
                            properties:
                                description:
                                    type: string
                                    example: Lékárna
                                group:
                                    type: string
                                    enum: [ pharmacies, health_care ]
                                    example: health_care
                                id:
                                    type: string
                                    example: Z
                            required:
                                - id
                    required:
                        - id
                        - institution_code
                        - name
                type:
                    type: string
                    example: Feature
            required:
                - geometry
                - type

        MedicalInstitutionType:
            type: object
            properties:
                pharmacies:
                    type: array
                    items:
                        type: string
                    example:
                        - Nemocniční lékárna s odbornými pracovišti
                        - Lékárna
                health_care:
                    type: array
                    items:
                        type: string
                    example:
                        - Fakultní nemocnice
                        - Nemocnice
