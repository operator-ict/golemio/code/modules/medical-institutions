# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.2.3] - 2024-12-03

### Added

-   asyncapi documentation ([ie#265](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/265))

## [1.2.2] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.2.1] - 2024-07-17

### Fixed

-   API validations ([core#109](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/109))

## [1.2.0] - 2024-06-03

### Added

-   The `Cache-Control` header to all output gateway responses ([core#105](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/105))

## [1.1.10] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.1.9] - 2024-04-08

### Fixed

-   API docs inconsistencies (https://gitlab.com/operator-ict/golemio/code/modules/medical-institutions/-/merge_requests/71)

### Changed

-   migration from axios to fetch ([core#99](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/99))

## [1.1.8] - 2024-03-25

### Fixed

-   Fixed router query validation rules ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [1.1.7] - 2023-09-27

### Changed

-   API versioning - version moved to path ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [1.1.6] - 2023-07-31

### Changed

-   Changed opening hours transformation that caused crashes because of missing opening hours ([medical-institutions#5](https://gitlab.com/operator-ict/golemio/code/modules/medical-institutions/-/issues/5))

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.1.4] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.1.3] - 2023-05-29

### Changed

-   Update openapi docs

## [1.1.2] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.1] - 2023-02-22

### Changed

-   Update city-districts

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.8] - 2022-11-29

### Changed

-   Mongo to PostgreSQL ([medical-institutions#3](https://gitlab.com/operator-ict/golemio/code/modules/medical-institutions/-/issues/3))

## [1.0.7] - 2022-09-06

### Changed

-   The Lodash library was replaced with js functions ([core#42](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/42))

## [1.0.6] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4
